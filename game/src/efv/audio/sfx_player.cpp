#include "efv/audio/sfx_player.h"

#include <SFML/Audio/SoundBuffer.hpp>

#include <iostream>
#include <array>
#include <algorithm>
#include <string_view>

namespace efv {

static constexpr std::array<const char*, 2> sound_files {{
    "./res/audio/a.ogg",
    "./res/audio/b.ogg"
}};

SfxPlayer::SfxPlayer()
{
    load(SoundEffect::Bell);
    load(SoundEffect::Boop);
}

void SfxPlayer::play(SoundEffect effect)
{
    sounds_.push_back(std::make_unique<sf::Sound>(sound_buffers_[effect]));
    sounds_.back()->play();
}

void SfxPlayer::load(SoundEffect effect)
{
    sf::SoundBuffer buffer;
    buffer.loadFromFile(sound_files[static_cast<std::size_t>(effect)]);

    sound_buffers_[effect] = buffer;
}

void SfxPlayer::update()
{
    sounds_.erase(std::remove_if(sounds_.begin(), sounds_.end(), [](std::unique_ptr<sf::Sound> const& s) {
        return s->getStatus() == sf::Sound::Stopped;
    }), sounds_.end());
}

}

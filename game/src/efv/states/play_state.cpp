#include "efv/states/play_state.h"
#include "efv/state_machine.h"
#include "efv/audio/sfx_player.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include "efv/state_machine.h"

namespace efv {

PlayState::PlayState(StateContext& context, bool replacing) : AbstractState(context, replacing) {
    rect_.setFillColor(sf::Color::White);
    rect_.setSize(sf::Vector2f(100.f, 100.f));
    rect_.setPosition((window().getSize().x / 2.f) - (rect_.getSize().x / 2.f),
                      (window().getSize().y / 2.f) - (rect_.getSize().y / 2.f));
}

PlayState::~PlayState() {}

void PlayState::pause() {}

void PlayState::resume() {}

void PlayState::update() {
    for(sf::Event event{}; window().pollEvent(event);) {
        if(event.type == sf::Event::Closed) {
            machine().quit();
        } else if(event.type == sf::Event::KeyPressed) {
            switch(event.key.code) {
                case sf::Keyboard::A:
                    context_.sfx_player.play(SfxPlayer::SoundEffect::Bell);
                    break;
                case sf::Keyboard::B:
                    context_.sfx_player.play(SfxPlayer::SoundEffect::Boop);
                    break;
                default:
                    break;
            }
        }
    }

    //TODO: Maybe have a centralized list of "processables" which is processed with delta time.
    context_.sfx_player.update();
}

void PlayState::draw() {
    window().clear();
    window().draw(rect_);
    window().display();
}

}  // namespace efv

#include "efv/states/abstract_state.h"

namespace efv {

AbstractState::AbstractState(StateContext& context, bool replace) : context_(context), is_replacing_(replace) {}

std::unique_ptr<AbstractState> AbstractState::nextState() { return std::move(next_state_); }

bool AbstractState::isReplacing() const { return is_replacing_; }

sf::RenderWindow& AbstractState::window() { return context_.window; }

StateMachine& AbstractState::machine() { return context_.machine; }

}  // namespace efv

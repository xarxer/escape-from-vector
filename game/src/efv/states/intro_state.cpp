#include "efv/states/intro_state.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include "efv/state_machine.h"
#include "efv/states/play_state.h"

namespace efv {

IntroState::IntroState(StateContext& context, bool replacing) : AbstractState(context, replacing) {
    font_.loadFromFile("./res/fonts/press_start_2p.ttf");

    title_text_.setFont(font_);
    title_text_.setCharacterSize(32);
    title_text_.setFillColor(sf::Color(200, 200, 200));
    title_text_.setString("ESCAPE FROM VECTOR");
    title_text_.setPosition((window().getSize().x / 2.f) - (title_text_.getGlobalBounds().width / 2.f), 300.f);

    instruction_text_.setFont(font_);
    instruction_text_.setCharacterSize(32);
    instruction_text_.setFillColor(sf::Color::White);
    instruction_text_.setString("Press any key");
    instruction_text_.setPosition((window().getSize().x / 2.f) - (instruction_text_.getGlobalBounds().width / 2.f),
                                  370.f);
}

IntroState::~IntroState() {}

void IntroState::pause() {}

void IntroState::resume() {}

void IntroState::update() {
    for(auto event = sf::Event{}; window().pollEvent(event);) {
        switch(event.type) {
            case sf::Event::Closed:
                machine().quit();
                break;
            case sf::Event::KeyPressed: {
                if(event.key.code != sf::Keyboard::Escape) {
                    next_state_ = StateMachine::BuildState<PlayState>(context_, true);
                } else {
                    context_.machine.quit();
                }
            } break;
            default:
                break;
        }
    }

    if(blink_clock_.getElapsedTime().asMilliseconds() >= 500) {
        instruction_visible_ = !instruction_visible_;
        blink_clock_.restart();
    }
}

void IntroState::draw() {
    window().clear();
    window().draw(title_text_);
    if(instruction_visible_) {
        window().draw(instruction_text_);
    }
    window().display();
}

}  // namespace efv

#include "efv/application.h"

#include "efv/states/intro_state.h"

namespace efv {

void Application::run()
{

    StateContext context{window_, machine_, sfx_player_};

    window_.create({1280, 720}, "Escape from Vector");
    window_.setFramerateLimit(60);

    machine_.run(StateMachine::BuildState<IntroState>(context, true));

    while(machine_.isRunning()) {
        machine_.nextState();
        machine_.update();
        machine_.draw();
    }
}

}  // namespace efv

#include "efv/state_machine.h"

namespace efv {

StateMachine::StateMachine() :
    log_(efv::Logger::Create("StateMachine"))
{}

void StateMachine::run(std::unique_ptr<AbstractState> state) {
    log_->info("Running");
    is_running_ = true;
    states_.push(std::move(state));
}

void StateMachine::nextState() {
    if(resume_) {
        if(!states_.empty()) {
            states_.pop();
        }

        if(!states_.empty()) {
            states_.top()->resume();
        }

        log_->info("Resuming state");
        resume_ = false;
    }

    if(!states_.empty()) {
        auto next_state = states_.top()->nextState();

        if(next_state != nullptr) {
            if(next_state->isReplacing()) {
                states_.pop();
            } else {
                states_.top()->pause();
            }

            states_.push(std::move(next_state));
            log_->info("State transition complete");
        }
    }
}

void StateMachine::lastState() { resume_ = true; }

void StateMachine::update() { states_.top()->update(); }

void StateMachine::draw() { states_.top()->draw(); }

bool StateMachine::isRunning() const { return is_running_; }

void StateMachine::quit() {
    log_->info("Quitting");
    is_running_ = false;
}

}  // namespace efv

#include "efv/log/logger.h"

#include <spdlog/sinks/stdout_color_sinks.h>

namespace efv {

std::vector<spdlog::sink_ptr> Logger::sinks_{};

std::shared_ptr<spdlog::logger> Logger::Create(std::string name)
{
    auto logger = spdlog::get(name);

    if(!logger) {
        if(sinks_.size() > 0) {
            logger = std::make_shared<spdlog::logger>(name, sinks_.begin(), sinks_.end());
            spdlog::register_logger(logger);
        } else {
            return spdlog::stdout_color_st(name);
        }
    }

    return logger;
}

void Logger::addSink(spdlog::sink_ptr sink)
{
    auto it = std::find(sinks_.cbegin(), sinks_.cend(), sink);

    if(it == sinks_.cend()) {
        sinks_.push_back(sink);
    }
}

void Logger::addSinks(const std::vector<spdlog::sink_ptr>& sinks)
{
    for(auto& sink : sinks) {
        addSink(sink);
    }
}

}

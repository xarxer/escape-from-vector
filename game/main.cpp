#include <iostream>

#include <spdlog/sinks/stdout_color_sinks.h>

#include "efv/application.h"
#include "efv/log/logger.h"

void initializeLogging() {
    efv::Logger::addSink(std::make_shared<spdlog::sinks::stdout_color_sink_st>());
}

int main(int argc, char* argv[]) {

    initializeLogging();
    auto logger = efv::Logger::Create("main");

    logger->info("Starting up");

    efv::Application app;
    app.run();

    logger->info("Shutting down");

    return 0;
}

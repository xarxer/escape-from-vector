#pragma once

#include <deque>
#include <memory>
#include <unordered_map>

#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

namespace efv {

class SfxPlayer
{

public:

    enum class SoundEffect {
        Bell,
        Boop
    };

    /*!
     * \brief Default constructor
     */
    SfxPlayer();

    /*!
     * \brief Play the given sound effect.
     * \param effect The \sa SoundEffect to play.
     */
    void play(SoundEffect effect);

    /*!
     * \brief Load the given sound effect into a sound buffer.
     * \param effect The \sa SoundEffect to load.
     */
    void load(SoundEffect effect);

    /*!
     * \brief Updates the currently playing sounds.
     *
     * Every invocation checks if any sound has finished playing, and removes it if true.
     */
    void update();

private:

    std::deque<std::unique_ptr<sf::Sound>> sounds_;
    std::unordered_map<SoundEffect, sf::SoundBuffer> sound_buffers_;

};

}

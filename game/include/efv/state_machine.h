#pragma once

#include <iostream>
#include <memory>
#include <stack>
#include <stdexcept>

#include "efv/state_context.h"
#include "efv/states/abstract_state.h"
#include "efv/log/logger.h"

namespace sf {
class RenderWindow;
}

namespace efv {

class StateMachine {
public:
    StateMachine();

    void run(std::unique_ptr<AbstractState> state);

    void nextState();
    void lastState();

    void update();
    void draw();

    [[nodiscard]] bool isRunning() const;

    void quit();

    template <typename TState>
    static std::unique_ptr<TState> BuildState(StateContext& context, bool replace = true);

private:
    std::stack<std::unique_ptr<AbstractState>> states_;

    bool resume_ = false;
    bool is_running_ = false;

    std::shared_ptr<spdlog::logger> log_;
};

template <typename TState>
std::unique_ptr<TState> StateMachine::BuildState(StateContext& context, bool replace) {
    static_assert(std::is_base_of_v<AbstractState, TState>, "TState must be derived from AbstractState");
    std::unique_ptr<TState> new_state = nullptr;

    try {
        new_state = std::make_unique<TState>(context, replace);
    } catch(std::runtime_error& ex) {
        // TODO: Replace with propper logging (spdlog)
        std::cout << "Failed to create new state\n";
        std::cout << ex.what() << std::endl;
    }

    return new_state;
}

}  // namespace efv

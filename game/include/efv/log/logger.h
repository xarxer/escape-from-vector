#pragma once

#include <vector>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/rotating_file_sink.h>

namespace efv {

class Logger {

public:

    static std::shared_ptr<spdlog::logger> Create(std::string name);
    static void addSink(spdlog::sink_ptr sink);
    static void addSinks(std::vector<spdlog::sink_ptr> const& sinks);

private:

    static std::vector<spdlog::sink_ptr> sinks_;

};

}

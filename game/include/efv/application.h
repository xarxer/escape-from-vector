#pragma once

#include <SFML/Graphics/RenderWindow.hpp>

#include "efv/state_machine.h"
#include "efv/audio/sfx_player.h"

namespace efv {

class Application {
public:
    void run();

private:
    sf::RenderWindow window_;
    StateMachine machine_;
    SfxPlayer sfx_player_;
};

}  // namespace efv

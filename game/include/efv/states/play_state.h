#pragma once

#include <SFML/Graphics/RectangleShape.hpp>

#include "efv/states/abstract_state.h"

namespace efv {

class PlayState : public AbstractState {
public:
    PlayState(StateContext& context, bool replacing = true);
    ~PlayState() override;

    void pause() override;
    void resume() override;

    void update() override;
    void draw() override;

private:
    sf::RectangleShape rect_;
};

}  // namespace efv

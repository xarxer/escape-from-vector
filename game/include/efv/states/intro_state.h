#pragma once

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>

#include "efv/states/abstract_state.h"

namespace efv {

class IntroState : public AbstractState {
public:
    IntroState(StateContext& context, bool replacing = true);
    ~IntroState() override;

    void pause() override;
    void resume() override;

    void update() override;
    void draw() override;

private:
    sf::Text title_text_;
    sf::Text instruction_text_;
    sf::Font font_;
    sf::Clock blink_clock_;

    bool instruction_visible_ = true;
};

}  // namespace efv

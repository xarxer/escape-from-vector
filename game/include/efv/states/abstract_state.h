#pragma once

#include <memory>

#include "efv/state_context.h"

namespace sf {
class RenderWindow;
}

namespace efv {

class AbstractState {
public:
    AbstractState(StateContext& context, bool replace = true);
    virtual ~AbstractState() = default;

    AbstractState(AbstractState const&) = delete;
    AbstractState& operator=(AbstractState const&) = delete;

    virtual void pause() = 0;
    virtual void resume() = 0;

    virtual void update() = 0;
    virtual void draw() = 0;

    std::unique_ptr<AbstractState> nextState();

    [[nodiscard]] bool isReplacing() const;

protected:
    sf::RenderWindow& window();
    StateMachine& machine();

    StateContext& context_;
    bool is_replacing_;

    std::unique_ptr<AbstractState> next_state_;
};

}  // namespace efv

#pragma once

namespace sf {
class RenderWindow;
}

namespace efv {

class StateMachine;
class SfxPlayer;

struct StateContext {
    sf::RenderWindow& window;
    StateMachine& machine;
    SfxPlayer& sfx_player;
};

}  // namespace efv
